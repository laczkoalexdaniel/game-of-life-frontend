import { Injectable } from '@angular/core';
import { SQUARE_SIZE, GRID_LINE_WIDTH, GRID_HEIGHT, GRID_WIDTH } from '../app.constants';

@Injectable({
  providedIn: 'root'
})
export class CanvasService {

  constructor() { }

  /**
   * Sums square plus grid line.
   * @returns number
   */
  sumSquarePlusGridLine(): number {
    return SQUARE_SIZE + GRID_LINE_WIDTH;
  }

  /**
   * Gets canvas height.
   * @returns number
   */
  getCanvasHeight(): number {
    return GRID_HEIGHT * this.sumSquarePlusGridLine() + 1;
  }

  /**
   * Gets canvas width.
   * @returns number
   */
  getCanvasWidth(): number {
    return GRID_WIDTH * this.sumSquarePlusGridLine() + 1;
  }

}
