import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { GameOfLifeService } from './game-of-life.service';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  private loadFile: Subject<any>;
  private nextGeneration: Subject<any>;
  private previousGeneration: Subject<any>;
  private continousGeneration: Subject<any>;
  private stopContinousGeneration: Subject<any>;
  private isContinousGenerationGoingOn: boolean;

  constructor(private gameOfLifeService: GameOfLifeService) {
    this.loadFile = new Subject<any>();
    this.nextGeneration = new Subject<any>();
    this.previousGeneration = new Subject<any>();
    this.continousGeneration = new Subject<any>();
    this.stopContinousGeneration = new Subject<any>();
    this.isContinousGenerationGoingOn = false;
   }

  /**
   * Sends load event.
   * @param fileName
   */
  sendLoadEvent(fileName: string): void {
    this.gameOfLifeService.setLoadFileName(fileName);

    if (this.isContinousGenerationGoingOn) {
      this.sendStopContinousGenerationEvent();
    }

    this.loadFile.next();
  }

  /**
   * Gets load event.
   * @returns Observable<any>
   */
  getLoadEvent(): Observable<any> {
     return this.loadFile.asObservable();
  }

  /**
   * Sends next generation event.
   */
  sendNextGenerationEvent(): void {
    this.gameOfLifeService.incrementGeneration();
    this.nextGeneration.next();
  }

  /**
   * Gets next generation event
   * @returns Observable<any>
   */
  getNextGenerationEvent(): Observable<any> {
     return this.nextGeneration.asObservable();
  }

  /**
   * Sends previous generation event
   */
  sendPreviousGenerationEvent(): void {
    this.gameOfLifeService.decrementGeneration();
    this.previousGeneration.next();
  }

  /**
   * Gets previous generation event
   * @returns Observable<any>
   */
  getPreviousGenerationEvent(): Observable<any> {
     return this.previousGeneration.asObservable();
  }

  /**
   * Sends continous generation event.
   */
  sendContinousGenerationEvent(): void {
    if (!this.isContinousGenerationGoingOn) {
      this.isContinousGenerationGoingOn = true;
      this.continousGeneration.next();
    }
  }

  /**
   * Gets continous generation event
   * @returns Observable<any>
   */
  getContinousGenerationEvent(): Observable<any> {
     return this.continousGeneration.asObservable();
  }

  /**
   * Sends stop continous generation event.
   */
  sendStopContinousGenerationEvent(): void {
    this.isContinousGenerationGoingOn = false;
    this.stopContinousGeneration.next();
  }

  /**
   * Gets stop continous generation event
   * @returns Observable<any>
   */
  getStopContinousGenerationEvent(): Observable<any> {
     return this.stopContinousGeneration.asObservable();
  }

  /**
   * Returns is continous generation going on.
   * @returns bool
   */
  getIsContinousGenerationGoingOn(): boolean {
    return this.isContinousGenerationGoingOn;
  }

  /**
   * Sets is continous generation going on.
   * @param value
   */
  setIsContinousGenerationGoingOn(value: boolean): void {
    this.isContinousGenerationGoingOn = value;
  }

}
