import { Injectable } from '@angular/core';
import { GameOfLifeService } from './game-of-life.service';
import { MAX_GENERATION_STORE } from '../app.constants';
import { EventService } from './event.service';

@Injectable({
  providedIn: 'root'
})
export class StoreGenerationService {

  private oldestGenerationStored: number;

  constructor(private gameOfLifeService: GameOfLifeService, private eventService: EventService) {
    this.eventService.getLoadEvent().subscribe(() => {
      this.oldestGenerationStored = 1;
    });
  }

  /**
   * Gets storage name
   * @param generation
   * @returns string
   */
  private getStorageName(generation: number): string {
    return 'g:' + generation;
  }

  /**
   * Removes not required generation
   */
  private removeNotRequiredGeneration(): void {
    if (MAX_GENERATION_STORE && this.gameOfLifeService.getGenerationCounter() > MAX_GENERATION_STORE) {
      let generationToRemove: number = this.gameOfLifeService.getGenerationCounter() - MAX_GENERATION_STORE;
      localStorage.removeItem(this.getStorageName(generationToRemove));
      this.oldestGenerationStored = generationToRemove + 1;
    }
  }

  /**
   * Removes generation when local storage full.
   */
  private removeGenerationsWhenLocalStorageFull(): void {
    for(let i = 1; i < 20; i++) {
      localStorage.removeItem(this.getStorageName(this.oldestGenerationStored + i));
      this.oldestGenerationStored++;
    }
  }

  /**
   * Sets generation in local storage.
   */
  private setGenerationInLocalStorage(): void {
    localStorage.setItem(
      this.getStorageName(this.gameOfLifeService.getGenerationCounter()),
      JSON.stringify(this.gameOfLifeService.getLiveCells())
    );
  }

  /**
   * Stores generation.
   */
  storeGeneration(): void {
    if (!this.checkGenerationIsStroed(this.gameOfLifeService.getGenerationCounter())) {
      this.removeNotRequiredGeneration();

      try {
        this.setGenerationInLocalStorage();
      }
      catch (exception) {
        this.removeGenerationsWhenLocalStorageFull();
        this.storeGeneration();
      }
    }
  }

  /**
   * Gets stored generation
   * @param generation
   * @returns String[]
   */
  getStoredGeneration(generation: number): String[] {
    return JSON.parse(localStorage.getItem(this.getStorageName(generation)));
  }

  /**
   * Checks generation is stroed
   * @param generation
   * @returns boolean
   */
  checkGenerationIsStroed(generation: number): boolean {
    return localStorage.hasOwnProperty(this.getStorageName(generation));
  }

}
