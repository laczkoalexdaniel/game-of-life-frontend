import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { BACKEND_API_URL, GRID_WIDTH, GRID_HEIGHT } from '../app.constants';

@Injectable({
  providedIn: 'root'
})
export class GameOfLifeService {

  private population: Subject<any>;
  private generation: Subject<any>;
  private generationCounter: number;
  private liveCells: String[];
  private loadFileName: string;

  constructor(private httpClient: HttpClient) {
    this.population = new Subject<any>();
    this.generation = new Subject<any>();
    this.generationCounter = 0;
  }

  /**
   * Gets population.
   * @returns Observable<any>
   */
  getPopulation(): Observable<any> {
    return this.population.asObservable();
  }

  /**
   * Sets population.
   * @param population
   */
  setPopulation(population: number): void {
    this.population.next(population);
  }

  /**
   * Gets generation
   * @returns Observable<any>
   */
  getGeneration(): Observable<any> {
    return this.generation.asObservable();
  }

  /**
   * Sets generation.
   * @param generation
   */
  setGeneration(generation: number): void {
    this.generationCounter = generation;
    this.generation.next(generation);
  }

  /**
   * Gets generation counter.
   * @returns number
   */
  getGenerationCounter(): number {
    return this.generationCounter;
  }

  /**
   * Increments generation.
   */
  incrementGeneration(): void {
    this.generationCounter++;
    this.generation.next(this.generationCounter);
  }

  /**
   * Decrements generation.
   */
  decrementGeneration(): void {
    this.generationCounter--;
    this.generation.next(this.generationCounter);
  }

  /**
   * Gets live cells.
   * @returns String[]
   */
  getLiveCells(): String[] {
    return this.liveCells;
  }

  /**
   * Sets live cells.
   * @param liveCells
   */
  setLiveCells(liveCells: String[]): void {
    this.liveCells = liveCells;
  }

  /**
   * Sets load file name.
   * @param loadFileName
   */
  setLoadFileName(loadFileName: string): void {
    this.loadFileName = loadFileName;
  }

  /**
   * Gets file list.
   * @returns Observable<Object>
   */
  getFileList(): Observable<Object> {
    return this.httpClient.get(BACKEND_API_URL + 'file-names');
  }

  /**
   * Inits game.
   * @returns Observable<Object>
   */
  initGame(): Observable<Object> {
    return this.httpClient.get(BACKEND_API_URL + 'parse-file/' + this.loadFileName);
  }

  /**
   * Gets next generation.
   * @returns Observable<Object>
   */
  getNextGeneration(): Observable<Object> {
    return this.httpClient.post(BACKEND_API_URL + 'next-generation/' + GRID_WIDTH + '-' + GRID_HEIGHT, this.liveCells);
  }

}
