import { TestBed } from '@angular/core/testing';

import { StoreGenerationService } from './store-generation.service';

describe('StoreGenerationService', () => {
  let service: StoreGenerationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StoreGenerationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
