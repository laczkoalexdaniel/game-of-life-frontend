import { Component, HostListener, OnInit } from '@angular/core';
import { EventService } from '../service/event.service';
import { GameOfLifeService } from '../service/game-of-life.service';
import { SHIFT_TIME_IN_MILLISECONDS } from '../app.constants';
import { StoreGenerationService } from '../service/store-generation.service';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {

  private population: number;
  private generation: number;
  private disablePrevious: boolean;
  private disableContinous: boolean;
  private disableNext: boolean;
  private waitAndDisable: boolean;

  constructor(
    private gameOfLifeService: GameOfLifeService, private eventService: EventService,
    private storeGenerationService: StoreGenerationService
  ) {
    this.gameOfLifeService.getPopulation().subscribe((population: number) => {this.population = population;});
    this.gameOfLifeService.setPopulation(0);
    this.gameOfLifeService.getGeneration().subscribe((generation: number) => {
      this.generation = generation;
      this.disablePrevious = generation == 1;
    });
    this.gameOfLifeService.setGeneration(0);
    this.disablePrevious = true;
    this.disableContinous = true;
    this.disableNext = true;
    this.waitAndDisable = false;

    this.eventService.getLoadEvent().subscribe(() => {
      this.disableContinous = false;
      this.disableNext = false;
    });
  }

  ngOnInit(): void {
  }

  /**
   * Gets population.
   * @returns number
   */
  getPopulation(): number {
    return this.population;
  }

  /**
   * Sets population.
   * @param population
   */
  setPopulation(population: number) {
    this.population = population;
  }

  /**
   * Gets generation
   * @returns number
   */
  getGeneration(): number {
    return this.generation;
  }

  /**
   * Sets generation.
   * @param generation
   */
  setGeneration(generation: number) {
    this.generation = generation;
  }

  /**
   * Gets disable previous.
   * @returns boolena
   */
  getDisablePrevious(): boolean {
    return this.disablePrevious || this.waitAndDisable;
  }

  /**
   * Sets disable previous.
   * @param disablePrevious
   */
  setDisablePrevious(disablePrevious: boolean) {
    this.disablePrevious = disablePrevious;
  }

  /**
   * Getdisables continous.
   * @returns boolean
   */
  getDisableContinous(): boolean {
    return this.disableContinous;
  }

  /**
   * Sets disable continous.
   * @param disableContinous
   */
  setDisableContinous(disableContinous: boolean) {
    this.disableContinous = disableContinous;
  }

  /**
   * Gets disable next.
   * @returns boolean
   */
  getDisableNext(): boolean {
    return this.disableNext || this.waitAndDisable;
  }

  /**
   * Sets disable next.
   * @param disableNext
   */
  setDisableNext(disableNext: boolean) {
    this.disableNext = disableNext;
  }

  /**
   * Previous button click.
   */
  onClickPrevious() {
    this.eventService.sendPreviousGenerationEvent();
    this.handleButtonWait();
    this.disablePrevious = !this.storeGenerationService.checkGenerationIsStroed(this.generation - 1);
  }

  /**
   * Continous button click.
   */
  onClickContinous() {
    this.eventService.getIsContinousGenerationGoingOn()
      ? this.eventService.sendStopContinousGenerationEvent()
      : this.eventService.sendContinousGenerationEvent();
  }

  /**
   * Next button click.
   */
  onClickNext() {
    this.eventService.sendNextGenerationEvent();
    this.handleButtonWait();
  }

  /**
   * Handles space keyboard event.
   * @param event
   */
  handleSpaceEvent(event: KeyboardEvent): void {
    if(event.key === ' ')
    {
      event.preventDefault();

      if (this.generation > 0) {
        this.eventService.getIsContinousGenerationGoingOn()
          ? this.eventService.sendStopContinousGenerationEvent()
          : this.eventService.sendContinousGenerationEvent();
      }
    }
  }

  private handleButtonWait(): void {
    this.waitAndDisable = true;

    setTimeout(() => {
      this.waitAndDisable = false;
    }, SHIFT_TIME_IN_MILLISECONDS);
  }

  /**
   * Handles arrow left keyboard event.
   * @param event
   */
  private handleArrowLeftEvent(event: KeyboardEvent): void {
    if(event.key === 'ArrowLeft')
    {
      event.preventDefault();

      if (this.generation > 1 && !this.waitAndDisable) {
        this.onClickPrevious();
      }
    }
  }

  /**
   * Handles arrow right keyboard event.
   * @param event
   */
  private handleArrowRightEvent(event: KeyboardEvent): void {
    if(event.key === 'ArrowRight')
    {
      event.preventDefault();

      if (this.generation > 0 && !this.waitAndDisable) {
        this.onClickNext();
      }
    }
  }

  /**
   * Handling keyboard event.
   * @param event
   */
  @HostListener('document:keydown', ['$event'])
  private handleKeyboardEvent(event: KeyboardEvent):void {
    this.handleArrowLeftEvent(event);
    this.handleArrowRightEvent(event);
    this.handleSpaceEvent(event);
  }

}
