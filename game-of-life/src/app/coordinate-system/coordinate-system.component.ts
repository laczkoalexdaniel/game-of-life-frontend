import { Component, OnInit } from '@angular/core';
import { CanvasService } from '../service/canvas.service';
import { GRID_LINE_WIDTH } from '../app.constants';

const HEIGHT = 1;
const WIDTH = 2;

@Component({
  selector: 'app-coordinate-system',
  templateUrl: './coordinate-system.component.html',
  styleUrls: ['./coordinate-system.component.css']
})
export class CoordinateSystemComponent implements OnInit {

  private canvasContext: CanvasRenderingContext2D;

  constructor(private canvasService: CanvasService) { }

  ngOnInit(): void {
  }

  ngAfterContentInit(): void {
    this.canvasContext = document.getElementsByTagName('canvas')[0].getContext('2d');
    this.drawCoordinateSystem();
  }

  /**
   * Draws coordinate system.
   */
  private drawCoordinateSystem(): void {
    let canvasHeight: number = this.canvasService.getCanvasHeight();
    let canvasWidth: number = this.canvasService.getCanvasWidth();

    this.canvasContext.canvas.height = canvasHeight;
    this.canvasContext.canvas.width = canvasWidth;
    this.canvasContext.lineWidth = GRID_LINE_WIDTH;
    this.canvasContext.translate(0.5, 0.5);

    this.drawLines(HEIGHT, canvasHeight);
    this.drawLines(WIDTH, canvasWidth);
  }

  /**
   * Gets line color for coordinate system.
   * @param position
   * @param canvasSize
   * @returns string
   */
  private getLineColor(position: number, canvasSize: number): string {
    let gridLineColor: string;

    if (position == ((canvasSize - 1) / 2)) {
      gridLineColor = 'rgba(0, 0, 0, 0.25)';
    }
    else if ((position / this.canvasService.sumSquarePlusGridLine()) % 10 == 0) {
      gridLineColor = 'rgba(0, 0, 0, 0.1)';
    }
    else {
      gridLineColor = 'rgba(220, 220, 220, 0.2)';
    }

    return gridLineColor;
  }

  /**
   * Yeah this is a mess.
   * @param heightOrWidth
   * @param heightOrWidthSize
   */
  private drawLines(heightOrWidth: number, heightOrWidthSize: number): void {
    for (let i = 0; i < heightOrWidthSize; i += this.canvasService.sumSquarePlusGridLine()) {
      this.canvasContext.beginPath();
      this.canvasContext.moveTo(heightOrWidth == HEIGHT ? i : 0, heightOrWidth == WIDTH ? i : 0);
      this.canvasContext.lineTo(
        heightOrWidth == HEIGHT ? i : heightOrWidthSize, heightOrWidth == WIDTH ? i : heightOrWidthSize
      );
      this.canvasContext.strokeStyle = this.getLineColor(i, heightOrWidthSize);
      this.canvasContext.stroke();
    }
  }

}
