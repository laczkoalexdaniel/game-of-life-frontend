import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadLifeFileComponent } from './load-life-file.component';

describe('LoadLifeFileComponent', () => {
  let component: LoadLifeFileComponent;
  let fixture: ComponentFixture<LoadLifeFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoadLifeFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadLifeFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
