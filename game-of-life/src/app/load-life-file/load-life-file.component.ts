import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EventService } from '../service/event.service';
import { GameOfLifeService } from '../service/game-of-life.service';

@Component({
  selector: 'app-load-life-file',
  templateUrl: './load-life-file.component.html',
  styleUrls: ['./load-life-file.component.css']
})
export class LoadLifeFileComponent implements OnInit {

  fileList: Object;
  loadFileFormGroup: FormGroup;

  constructor(
    private gameOfLifeService: GameOfLifeService, private formBuilder: FormBuilder, private eventService: EventService
  ) {}

  ngOnInit(): void {
    this.gameOfLifeService.getFileList().subscribe(data => {
      this.fileList = data;
    });

    this.loadFileFormGroup = this.formBuilder.group({
      file: ['']
    });
  }

  /**
   * Form submit.
   */
  onSubmit() {
    this.eventService.sendLoadEvent(this.loadFileFormGroup.get('file').value);
  }

}
