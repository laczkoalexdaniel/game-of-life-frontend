import { Injectable } from '@angular/core';

export const SQUARE_SIZE: number = 10;
export const GRID_LINE_WIDTH: number = 2;
export const GRID_HEIGHT:number = 360;
export const GRID_WIDTH: number = 360;
export const BACKEND_API_URL: string = 'http://localhost:8080/api/';
export const MAX_GENERATION_STORE: number = 100; //zero means limit is sthe localStorage limit
export const SHIFT_TIME_IN_MILLISECONDS: number = 200;


@Injectable({
    providedIn: 'root'
})
export class AppConstants {

}
