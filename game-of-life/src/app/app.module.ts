import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoordinateSystemComponent } from './coordinate-system/coordinate-system.component';
import { LoadLifeFileComponent } from './load-life-file/load-life-file.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { LiveCellsComponent } from './live-cells/live-cells.component';

@NgModule({
  declarations: [
    AppComponent,
    CoordinateSystemComponent,
    LoadLifeFileComponent,
    NavigationBarComponent,
    LiveCellsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
