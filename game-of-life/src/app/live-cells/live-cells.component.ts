import { Component, OnInit } from '@angular/core';
import { CanvasService } from '../service/canvas.service';
import { GameOfLifeService } from '../service/game-of-life.service';
import { SQUARE_SIZE, SHIFT_TIME_IN_MILLISECONDS } from '../app.constants';
import { timer } from 'rxjs';
import { StoreGenerationService } from '../service/store-generation.service';
import { EventService } from '../service/event.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-live-cells',
  templateUrl: './live-cells.component.html',
  styleUrls: ['./live-cells.component.css']
})
export class LiveCellsComponent implements OnInit {

  private canvasContext: CanvasRenderingContext2D;

  constructor(
    private canvasService: CanvasService, private gameOfLifeService: GameOfLifeService,
    private storeGenerationService: StoreGenerationService, private eventService: EventService
  ) {
    this.eventService.getLoadEvent().subscribe(() => {
      this.gameOfLifeService.initGame().subscribe((liveCells: String[]) => {
        this.setLiveCellsAndPopulation(liveCells);
        this.gameOfLifeService.setGeneration(1);
        localStorage.clear();
        this.storeGenerationService.storeGeneration();
        this.drawLiveCells();
      });
    });

    this.eventService.getNextGenerationEvent().subscribe(() => {
      this.drawNextGeneration();
    });

    this.eventService.getPreviousGenerationEvent().subscribe(() => {
      this.setLiveCellsAndPopulation(
        this.storeGenerationService.getStoredGeneration(this.gameOfLifeService.getGenerationCounter())
      );
      this.drawLiveCells();
    });

    this.eventService.getContinousGenerationEvent().subscribe(() => {
      timer(500, SHIFT_TIME_IN_MILLISECONDS).pipe(takeUntil(this.eventService.getStopContinousGenerationEvent()))
        .subscribe(() => {
          this.gameOfLifeService.incrementGeneration();
          this.drawNextGeneration();
      });
    });
  }

  ngOnInit(): void {
  }

  ngAfterContentInit(): void {
    this.canvasContext = document.getElementsByTagName('canvas')[1].getContext('2d');
    this.canvasContext.canvas.height = this.canvasService.getCanvasHeight();
    this.canvasContext.canvas.width = this.canvasService.getCanvasWidth();
    this.setOrigo();
  }

  /**
   * Gets origo.
   * @returns number[]
   */
  private getOrigo(): number[] {
    return [this.canvasService.getCanvasWidth() / 2, this.canvasService.getCanvasHeight() / 2];
  }

  /**
   * Sets origo for the canvas.
   */
  private setOrigo(): void {
    let origo: number[] = this.getOrigo();
    this.canvasContext.translate(origo[0] + 1, origo[1] + 1);
  }

  /**
   * Clears canvas.
   */
  private clearCanvas(): void {
    let canvasWidth: number = this.canvasService.getCanvasWidth();
    let canvasHeight: number = this.canvasService.getCanvasHeight();
    this.canvasContext.clearRect(-canvasWidth / 2, -canvasHeight / 2, canvasWidth, canvasHeight);
  }

  /**
   * Draws the live cells on the canvas.
   */
  private drawLiveCells(): void {
    this.clearCanvas();
    this.gameOfLifeService.getLiveCells().forEach(coordinate => {
      let splittedCoordinate = coordinate.split(" ");
      this.drawSquare(this.convertCoordinate([Number(splittedCoordinate[0]), Number(splittedCoordinate[1])]));
    });
  }

  /**
   * Converts coordinate for canvas.
   * @param coordinate
   * @returns number[]
   */
  private convertCoordinate(coordinate: number[]): number[] {
    return [
      (coordinate[0] * this.canvasService.sumSquarePlusGridLine()),
      -(coordinate[1] * this.canvasService.sumSquarePlusGridLine())
    ];
  }

  /**
   * Draws square on the canvas.
   * @param coordinate
   */
  private drawSquare(coordinate: number[]): void {
    this.canvasContext.beginPath();
    this.canvasContext.rect(coordinate[0], coordinate[1], SQUARE_SIZE, SQUARE_SIZE);
    this.canvasContext.fillStyle = 'black';
    this.canvasContext.fill();
    this.canvasContext.stroke();
  }

  /**
   * Sets live cells and population on GameOfLifeService.
   * @param liveCells
   */
  private setLiveCellsAndPopulation(liveCells: String[]): void {
    this.gameOfLifeService.setLiveCells(liveCells);
    this.gameOfLifeService.setPopulation(liveCells.length);
  }

  /**
   * Draws next generation on canvas.
   */
  private drawNextGeneration(): void {
    let nextGeneration: String[] = this.storeGenerationService.getStoredGeneration(
      this.gameOfLifeService.getGenerationCounter()
    );

    if (nextGeneration) {
      this.setLiveCellsAndPopulation(nextGeneration);
      this.drawLiveCells();
    } else {
      this.gameOfLifeService.getNextGeneration().subscribe((liveCells: String[]) => {
        this.setLiveCellsAndPopulation(liveCells);
        this.storeGenerationService.storeGeneration();
        this.drawLiveCells();
      });
    }
    //I know I should put this.drawLiveCells() here but when I do it produces a weird bug and I can't figure it out.
    //It runs before the if block, if you know why pls tell me thx. :)
  }

}
