import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveCellsComponent } from './live-cells.component';

describe('LiveCellsComponent', () => {
  let component: LiveCellsComponent;
  let fixture: ComponentFixture<LiveCellsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiveCellsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveCellsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
